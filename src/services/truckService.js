const { Truck } = require('../model/Truck.js');

function getDimension(type) {
    switch (type) {
        case 'SPRINTER': {
            return {
                dimensions: {
                    width: 250,
                    length: 300,
                    height: 170,
                },
                payload: 1700,
            };
        }
        case 'SMALL STRAIGHT': {
            return {
                dimensions: {
                    width: 250,
                    length: 500,
                    height: 170,
                },
                payload: 2500,
            };
        }
        case 'LARGE STRAIGHT': {
            return {
                dimensions: {
                    width: 350,
                    length: 700,
                    height: 200,
                },
                payload: 4000,
            };
        }
        default: {
            return undefined;
        }
    }
}

const getAssignedTrucks = async ({ payload, dimensions }) => {
    return await Truck.findOne({
        assigned_to: { $ne: 'null' },
        status: { $eq: 'IS' },
        payload: { $gt: payload },
        'dimensions.width': { $gt: dimensions.width },
        'dimensions.length': { $gt: dimensions.length },
        'dimensions.height': { $gt: dimensions.height },
    });

};

module.exports = {
    getDimension,
    getAssignedTrucks
}