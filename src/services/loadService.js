function changeLoadState(state) {
    switch (state) {
        case 'En route to Pick Up':
            return 'Arrived to Pick Up';

        case 'Arrived to Pick Up':
            return 'En route to delivery';

        case 'En route to delivery':
            return 'Arrived to delivery';

        default:
            break;
    }
}

module.exports = {
    changeLoadState
}
