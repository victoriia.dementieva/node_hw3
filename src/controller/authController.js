const { User } = require('../model/User.js');
const mongoose = require('mongoose');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


function randomPassword() {
    const passwordLength = 8;

    let charSet = "0123456789abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let password = "";

    for (var i = 0; i <= passwordLength; i++) {
        var randomNumber = Math.floor(Math.random() * charSet.length);
        password += charSet.substring(randomNumber, randomNumber + 1);
    }

    return password;
}

const registerUser = async (req, res, next) => {
    try {
        const { email, password, role } = req.body;
        const user = new User({
            email,
            role,
            password: await bcrypt.hash(password, 10)
        });
        
        user.save()
            .then(() => res.json(
                {
                    "message": "Profile created successfully"
                }))
            .catch(err => {
                next(err);
            });
    } catch (err) {
        console.log(err);
    }
}

const loginUser = async (req, res, next) => {
    const user = await User.findOne({ email: req.body.email });

    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
        const payload = {
            _id: user._id,
            email: user.email,
            role: user.role
        };
        const jwtToken = jwt.sign(payload, 'secret-jwt-key');
        return res.status(200).json({
            "jwt_token": jwtToken
        });
    } else res.status(400).json({ 'message': 'Not authorized' });

}

const forgotPassword = async (req, res, next) => {
    const user = await User.findOne({ email: req.body.email });
    const newPassword = randomPassword();
    if (user) {
        user.password = await bcrypt.hash(newPassword, 10);
        user.save().then(() => res.status(200).json({
            "message": "New password sent to your email address"
        })).catch(err => {
            next(err);
        });
    } else {
        res.status(400).send({
            "message": "User with this email does not exist",
        });
    }
}
module.exports = {
    registerUser,
    loginUser,
    forgotPassword
};
