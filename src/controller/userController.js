const bcrypt = require('bcryptjs');

const { User } = require('../model/User.js');
const { Load } = require('../model/Load.js');
const { Truck } = require('../model/Truck.js');

function getProfile(req, res, next) {
    res.status(200).json(
        {
            "user": {
                "_id": req.user._id,
                "role": req.user.role,
                "email": req.user.email,
                "createdDate": new Date()
            }
        });
}

function deleteProfile(req, res, next) {
    if (req.user.role === 'SHIPPER') {
        const load = Load.deleteMany({
            userId: req.user._id,
        });

        if (load) {
            return User.findByIdAndDelete(req.user._id)
                .then((user) => {
                    user ?
                        res.status(200).status(200).send({
                            "message": 'Success'
                        }) :
                        res.status(400).send({
                            "message": "Can't find a user",
                        });
                });
        }
    } else {
        const truck = Truck.deleteMany({
            userId: req.user._id,
        });

        if (truck) {
            return User.findByIdAndDelete(req.user._id)
                .then((user) => {
                    user ?
                        res.status(200).status(200).send({
                            "message": 'Success'
                        }) :
                        res.status(400).send({
                            "message": "Can't find a user",
                        });
                });
        }
    }

    return res.status(400).send({
        "message": 'Something went wrong'
    })
}

async function changePassword(req, res, next) {
    const user = await User.findById(req.user._id);
    const { oldPassword, newPassword } = req.body;
    if (!oldPassword || !newPassword) {
        return res.status(400).send({
            "message": 'enter the password',
        });
    }
    if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
        user.password = await bcrypt.hash(newPassword, 10);
        return user.save().then(() => res.status(200).send({
            "message": "Success"
        })).catch(err => {
            next(err);
        });
    } else {
        return res.status(400).send({
            "message": "the old password is wrong",
        });
    }
}

module.exports = {
    getProfile,
    deleteProfile,
    changePassword
}