const { Truck } = require('../model/Truck.js');
const { getDimension } = require('../services/truckService');

const getTrucks = (req, res, next) => {
    if (req.user.role === 'DRIVER') {
        return Truck.find({
            created_by: req.user._id
        }, '-__v')
            .then((trucks) => {
                res.status(200).json({ "trucks": trucks });
            });
    }
    else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }
}

const addTruck = (req, res, next) => {
    const { type } = req.body;
    const { dimensions, payload } = getDimension(type);
    if (req.user.role === 'DRIVER') {
        if (type) {
            const truck = new Truck({
                created_by: req.user._id,
                type,
                status: "IS",
                created_date: new Date(),
                dimensions,
                payload
            });
            truck.save().then(() => {
                res.status(200).send({
                    "message": "Truck created successfully."
                });
            })
        } else {
            res.status(400).send({
                "message": "Enter the type of truck."
            });
        }
    } else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }

}

const getTruckById = (req, res, next) => {
    if (req.user.role === 'DRIVER') {
        Truck.findById(req.params.id)
            .then((truck) => {
                truck ?
                    res.status(200).json({ "truck": truck }) :
                    res.status(400).json({
                        "message": "No trucks with the following ID."
                    });
            });
    } else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }
}



const updateTruckById = (req, res, next) => {
    if (req.user.role === 'DRIVER') {
        const { type } = req.body;
        const { dimensions, payload } = getDimension(type);
        if (!type) {
            return res.status(400).send({
                "message": "The type field is required.",
            });
        }

        return Truck.findByIdAndUpdate({
            _id: req.params.id, userId: req.user.userId
        }, {
            $set: {
                type,
                dimensions,
                payload
            }
        })
            .then((truck) => {
                !truck ? res.status(400).send({
                    "message": "No trucks with the following ID."
                }) :
                    res.status(200).send({
                        "message": "Truck details changed successfully"
                    });
            })
    }
    else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }
}

const deleteTruckById = (req, res, next) => {
    if (req.user.role === 'DRIVER') {
        Truck.findByIdAndDelete(req.params.id)
            .then((truck) => {
                !truck ? res.status(400).send({
                    "message": "No trucks with the following ID."
                }) :
                    res.status(200).json({
                        "message": "Truck deleted successfully"
                    });
            });
    } else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }
}

const assignTruckToUser = async (req, res, next) => {
    const { id } = req.params;
    const isAssigned = await Truck.find({ assigned_to: req.user._id }).exec();
    const userTrucks = await Truck.find({ created_by: req.user._id }, '-__v').exec();

    if (req.user.role === 'DRIVER') {
        if (!id) {
            res.status(400).send({
                "message": "Truck ID is required."
            });
        }

        if (!userTrucks.length) {
            res.status(400).send({
                "message": "No trucks created."
            });
        }

        if (!isAssigned.length) {
            await Truck.findOneAndUpdate({
                _id: req.params.id,
                created_by: req.user._id
            }, {
                $set: {
                    assigned_to: req.user._id
                }
            });

            return res.status(200).send({
                "message": 'Truck assigned successfully',
            });
        } else {
            res.status(400).send({
                "message": "You can't assign more than one truck to yourself."
            });
        }


    } else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }
}


module.exports = {
    getTrucks,
    addTruck,
    getTruckById,
    updateTruckById,
    deleteTruckById,
    assignTruckToUser,
}