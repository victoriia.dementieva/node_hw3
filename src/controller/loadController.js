const { User } = require('../model/User.js');
const { Load } = require('../model/Load.js');
const { Truck } = require('../model/Truck.js');

const { changeLoadState } = require('../services/loadService');
const { getAssignedTrucks } = require('../services/truckService');

const getLoads = async (req, res, next) => {
    return Load.find({
        created_by: req.user._id,
        userId: req.user.userId
    }, '-__v')
        .then((loads) => {
            res.status(200).json({ "loads": loads });
        });
};

function addLoad(req, res, next) {
    const { name,
        pickup_address,
        delivery_address, payload, dimensions } = req.body;

    const load = new Load({
        created_by: req.user._id,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    });

    if (req.user.role === 'SHIPPER') {
        if (!load || !name || !payload || !pickup_address || !delivery_address || !dimensions) {
            res.status(400).send({
                "message": "Not all required fields"
            });
        } else {
            load.save().then(() => {
                res.status(200).json(
                    {
                        "message": "Load created successfully"
                    });
            });
        }
    } else {
        res.status(400).send({
            "message": "You are not a SHIPPER."
        });
    }

}

const getActive = async (req, res, next) => {
    if (req.user.role === 'DRIVER') {
        const activeLoads = await Load.findOne({
            assigned_to: req.user._id,
            status: { $ne: 'SHIPPED' },
        });

        res.status(200).send({
            "load": activeLoads
        })
    } else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        });
    }
}

const toNextState = async (req, res, next) => {
    if (req.user.role === 'DRIVER') {
        const load = await Load.findOne({ assigned_to: req.user._id }).exec();

        const newState = changeLoadState(load.state);

        let updatedLoad;

        !load ? res.status(400).send({
            "message": "You don't have active loads."
        }) : null;

        if (newState === undefined) {
            res.status(400).send({
                "message": "The load was shipped"
            })
        }
        if (newState === 'Arrived to delivery') {

            updatedLoad = await Load.findOneAndUpdate({}, {
                $set: {
                    state: newState,
                    status: 'SHIPPED'
                },
            });
            res.status(200).send({
                "message": `Load state changed to ${newState}`
            });
        } else {
            updatedLoad = await Load.findOneAndUpdate({}, {
                $set: {
                    state: newState,
                },
            });
            res.status(200).send({
                "message": `Load state changed to ${newState}`
            });
        }
    } else {
        res.status(400).send({
            "message": "You are not a DRIVER."
        })
    }
}

const getLoadById = (req, res, next) =>

    Load.findById(req.params.id)
        .then((load) => {
            load ?
                res.status(200).json({ "load": load }) :
                res.status(400).json({
                    "message": "No loads with the following ID."
                });
        });

const updateLoadById = (req, res, next) => {
    const { name,
        pickup_address,
        delivery_address,
    } = req.body;

    if (!name || !payload || !pickup_address || !delivery_address || !dimensions) {
        return res.status(400).send({
            "message": "Not all required fields",
        });
    }

    return Load.findByIdAndUpdate({
        _id: req.params.id, userId: req.user.userId
    }, {
        $set: {
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions
        }
    })
        .then((load) => {
            !load ? res.status(400).send({
                "message": "No loads with the following ID."
            }) :
                res.status(200).send({
                    "message": "Load details changed successfully"
                });
        })

}

const deleteLoadById = (req, res, next) =>
    Load.findByIdAndDelete(req.params.id)
        .then((load) => {
            !load ? res.status(400).send({
                "message": "No load with the following ID."
            }) :
                res.status(200).json({
                    "message": "Load deleted successfully"
                });
        });

const postLoadById = async (req, res, next) => {
    try {
        if (req.user.role !== 'SHIPPER') {
            res.status(400).send({
                "message": "You are not a SHIPPER."
            });
        }
        const { id } = req.params;

        if (!id) {
            return res.status(400)
                .send({
                    "message": 'Please specify load ID',
                });
        }
        const load = await Load.findOne({
            _id: id,
            created_by: req.user._id
        });

        if (!load || load.status !== 'NEW') {
            return res.status(400).send({
                "message": 'Load not found',
            });
        }

        Load.findOneAndUpdate({
            _id: id,
            created_by: req.user._id
        }, {
            $set: { status: 'POSTED' },
        });

        const truck = await getAssignedTrucks({
            payload: load.payload,
            dimensions: load.dimensions,
        });
        console.log(truck);
        if (!truck) {
            await Load.findOneAndUpdate({
                _id: id,
                created_by: req.user._id,
            }, {
                $set: { status: 'NEW' },
            });

            return res.status(400).send({
                "message": 'Driver not found',
            });
        }

        await Truck.findOneAndUpdate({
            _id: truck._id,
        }, {
            $set: { status: 'OL' },
        });

        await Load.findOneAndUpdate({
            _id: id,
            created_by: req.user._id,
        }, {
            $set: {
                logs: [
                    {
                        "message": `Load assigned to driver with id: ${truck.assigned_to}`,
                        time: new Date(),
                    },
                ],
                status: 'ASSIGNED',
                state: 'En route to Pick Up',
                assigned_to: truck.assigned_to,
            },
        });

        return res.status(200).send({
            "message": 'Load posted successfully',
            "driver_found": true
        });

    } catch (err) {
        return res.status(500).send({
            "message": err.message,
        });
    }

}

const getShippingInfoById = async (req, res, next) => {
    try {
        if (req.user.role !== 'SHIPPER') {
            res.status(400).send({
                "message": "You are not a SHIPPER."
            });
        }
        const { id } = req.params;
        if (!id) {
            return res.status(400).send({
                "message": 'Load ID is required.',
            });
        }

        const load = await Load.findOne({
            _id: id,
            created_by: req.user._id,
            assigned_to: { $exists: true },
        });

        if (!load) {
            return res.status(400).send({
                "message": 'We cannot find load with such ID',
            });
        }

        const truck = await Truck.findOne({
            assigned_to: load.assigned_to,
        });

        if (!truck) {
            return res.status(400).send({
                "message": 'wrong',
            });
        }

        return res.status(200).send({
            load,
            truck,
        });
    } catch (err) {
        return res.status(500).send({
            "message": err.message
        });
    }

}
module.exports = {
    getLoads,
    addLoad,
    getActive,
    toNextState,
    getLoadById,
    updateLoadById,
    deleteLoadById,
    postLoadById,
    getShippingInfoById
}