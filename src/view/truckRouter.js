const express = require('express');
const router = express.Router();
const {
    getTrucks,
    addTruck,
    getTruckById,
    updateTruckById,
    deleteTruckById,
    assignTruckToUser,
} = require('../controller/truckController.js');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getTrucks);

router.post('/', authMiddleware, addTruck);

router.get('/:id', authMiddleware, getTruckById);

router.put('/:id', authMiddleware, updateTruckById);

router.delete('/:id', authMiddleware, deleteTruckById);

router.post('/:id/assign', authMiddleware, assignTruckToUser);

module.exports = {
    truckRouter: router,
};
