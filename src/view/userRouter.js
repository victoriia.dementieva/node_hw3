const express = require('express');
const router = express.Router();
const { getProfile, deleteProfile, changePassword } = require('../controller/userController.js');
const { authMiddleware } = require('../middleware/authMiddleware');


router.get('/', authMiddleware, getProfile);

router.delete('/', authMiddleware, deleteProfile);

router.patch('/password', authMiddleware, changePassword);

module.exports = {
    userRouter: router,
};
