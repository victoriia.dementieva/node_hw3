const express = require('express');
const router = express.Router();
const { getLoads,
    addLoad,
    getActive,
    toNextState,
    getLoadById,
    updateLoadById,
    deleteLoadById,
    postLoadById,
    getShippingInfoById
} = require('../controller/loadController.js');

const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getLoads);
router.post('/', authMiddleware, addLoad);
router.get('/active', authMiddleware, getActive);
router.patch('/active/state', authMiddleware, toNextState);
router.get('/:id', authMiddleware, getLoadById);
router.put('/:id', authMiddleware, updateLoadById);
router.delete('/:id', authMiddleware, deleteLoadById);
router.post('/:id/post', authMiddleware, postLoadById);
router.get('/:id/shipping_info', authMiddleware, getShippingInfoById);

module.exports = {
    loadRouter: router,
};
