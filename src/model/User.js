const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    role: {
        type: String,
        enum: [
            'DRIVER',
            'SHIPPER'
        ],
        required: true
    },
    email: {
        type: String
    },
    password: {
        type: String,
        required: true
    },
    created_date: {
        type: String
    }

}
);
const User = mongoose.model('User', UserSchema);

module.exports = {
    User
};
