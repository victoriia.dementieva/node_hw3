const mongoose = require('mongoose');

const TruckSchema = mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId
    },
    assigned_to: {
        type: String
    },
    type: {
        type: String,
        enum: [
            'SPRINTER',
            'SMALL STRAIGHT',
            'LARGE STRAIGHT'
        ]
    },
    status: {
        type: String,
        enum: [
            'OL',
            'IS'
        ]
    },
    created_date: {
        type: String
    }

}
);
const Truck = mongoose.model('Truck', TruckSchema);

module.exports = {
    Truck
};
