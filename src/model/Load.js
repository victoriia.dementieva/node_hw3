const mongoose = require('mongoose');

const LoadSchema = mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId
    },
    status: {
        type: String,
        enum: [
            'NEW',
            'POSTED',
            'ASSIGNED',
            'SHIPPED'
        ],
        default: 'NEW'
    },
    state: {
        type: String,
        enum: [
            'En route to Pick Up',
            'Arrived to Pick Up',
            'En route to delivery',
            'Arrived to delivery'
        ]
    },
    name: {
        type: String
    },
    payload: {
        type: Number
    },
    pickup_address: {
        type: String
    },
    delivery_address: {
        type: String
    },
    dimensions: {
        width: {
            type: Number
        },
        length: {
            type: Number
        },
        height: {
            type: Number
        },
    },
    created_date: {
        type: String
    }
});
const Load = mongoose.model('Load', LoadSchema);

module.exports = {
    Load
};
