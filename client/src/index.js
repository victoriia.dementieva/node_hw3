import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import TrucksStore from './store/TrucksStore';
import LoadsStore from './store/LoadsStore';
import UserStore from './store/UserStore';

export const Context = createContext(null);
console.log(process.env.REACT_APP_API_URL);
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Context.Provider value={
    {
      user: new UserStore(),
      loads: new LoadsStore(),
      trucks: new TrucksStore()
    }
  }>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Context.Provider>

);


