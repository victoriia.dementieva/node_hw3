import { makeAutoObservable } from 'mobx';

export default class LoadsStore {
    constructor() {
        this._loads = [];
        makeAutoObservable(this);
    }

    setLoad(load) {
        this._loads = load;
    }

    get loads() {
        return this._loads;
    }
}