import { makeAutoObservable } from 'mobx';

export default class TrucksStore {
    constructor() {
        this._trucks = [];
        makeAutoObservable(this);
    }

    setTruck(truck) {
        this._trucks = truck;
    }

    get trucks() {
        return this._trucks;
    }
}