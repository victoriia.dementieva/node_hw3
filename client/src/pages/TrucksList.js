import TrucksBar from "../components/TrucksBar";
import { Container } from "react-bootstrap";
import { getTrucks } from "../http/truckAPI";
import { useContext, useEffect } from "react";
import { Context } from "..";

function TrucksList(params) {
    const { trucks } = useContext(Context);

    useEffect(() => {
        getTrucks().then(data => {
            trucks.setTruck(data.trucks);
        });
    }, []);
    return (
        <Container>
            <TrucksBar />
        </Container>
    );
};
export default TrucksList;