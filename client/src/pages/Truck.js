import { Button, Col, Container, Image, Row } from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import img from '../assets/truck.png';
import { observer } from "mobx-react-lite";
import { useParams, useNavigate } from "react-router-dom";
import { deleteTruckById} from "../http/truckAPI";
import { TRUCK_ROUTE } from '../utils/constants';
import {useTranslation} from "react-i18next";

const Truck = observer(() => {
    const { id } = useParams();
    const navigate = useNavigate();
    const { t } = useTranslation();
    
    return (
        <Container>
            <Row style={{
                border: '1px solid #fff',
                borderRadius: '50px',
                background: "linear-gradient(to left,#a7c4af, #fff)"
            }}
                className={'d-flex justify-content-between'}>
                <div className={'col-3'}>
                    <Image width={300} height={300}

                        src={img} />
                </div>

                <Col className={'text-center col-8 p-4 '}>
                    <h2>{`Truck № ${id}`}</h2>
                    <Form.Control
                        value={`text`}
                        disabled />
                </Col>
                <Col>
                    <Button>{t('truck.edit')}</Button>
                    <Button>{t('truck.assign')}</Button>
                    <Button onClick={() => {
                        deleteTruckById(id);
                        navigate(TRUCK_ROUTE);
                    }}>{t('truck.delete')}</Button>
                </Col>
            </Row>
        </Container>
    );
});

export default Truck;