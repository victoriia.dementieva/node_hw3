import { useContext, useState } from "react";
import { Container, Form, Button, NavLink, Col } from "react-bootstrap";
import Card from 'react-bootstrap/Card';
import { useLocation, useNavigate } from "react-router-dom";
import { login, registration, getUserProfile } from "../http/userAPI";
import {
    LOGIN_ROUTE,
    LOAD_ROUTE,
    FORGOT_ROUTE,
    TRUCK_ROUTE,
    REGISTRATION_ROUTE
} from '../utils/constants';
import { Context } from "..";
import { observer } from "mobx-react-lite";

const Auth = observer(() => {
    const { user } = useContext(Context);
    const location = useLocation();
    const isLogin = location.pathname === LOGIN_ROUTE;
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('');

    const click = async () => {
        try {
            let data;
            if (isLogin) {
                data = await login(email, password);
                const userData = await getUserProfile();
                user.setUser(userData.user);
                user.setIsAuth(true);
                userData.user.role === 'SHIPPER' ?
                    navigate(LOAD_ROUTE) : navigate(TRUCK_ROUTE);
            } else {
                data = await registration(email, password, role);
                navigate(LOGIN_ROUTE);
            }

        } catch (err) {
            alert(err.response.data.message);
        }
    }
    return (
        <Container className="d-flex justify-content-center align-items-center"
            style={{ height: window.innerHeight - 54 }}>
            <Card style={{ width: 700 }} className="p-5">
                <h2 className="m-auto">{isLogin ? 'Authorization' : 'Registration'}</h2>
                <Form className="d-flex flex-column">
                    <Form.Control
                        placeholder="Enter email"
                        className="mt-2"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Control
                        placeholder="Enter your password"
                        className="mt-2"
                        type="password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                    {!isLogin ?
                        <Form.Label htmlFor="select" >
                            <Form.Select id="select"
                                aria-label="Default select example"
                                className="mt-2"
                                value={role}
                                onChange={e => setRole(e.target.value)}
                                required>
                                <option>Nothing selected</option>
                                <option value="SHIPPER">SHIPPER</option>
                                <option value="DRIVER">DRIVER</option>
                            </Form.Select>
                        </Form.Label> : null}

                    <Col className="d-flex justify-content-between mt-3 pl-3 pr-3">
                        {isLogin ?
                            <div>Don't have an account?
                                <NavLink style={{ color: "green", textDecoration: "underline" }}
                                    to={REGISTRATION_ROUTE}
                                    onClick={() => { navigate(REGISTRATION_ROUTE); }}>
                                    Register
                                </NavLink>
                                <NavLink style={{ color: "green", textDecoration: "underline" }}
                                    to={FORGOT_ROUTE}
                                    onClick={() => { navigate(FORGOT_ROUTE); }}>
                                    Forgot password
                                </NavLink>
                            </div> :
                            <div>
                                Already have an account?
                                <NavLink style={{ color: "green", textDecoration: "underline" }}
                                    to={LOGIN_ROUTE}
                                    onClick={() => { navigate(LOGIN_ROUTE); }}>
                                    Log In
                                </NavLink>

                            </div>
                        }

                        {isLogin ?
                            <Button variant="outline-success" className="mb-3" onClick={click}>
                                Log in
                            </Button> :
                            <Button variant="outline-success" className="mb-3" onClick={click}>
                                Register
                            </Button>
                        }

                    </Col>

                </Form>
            </Card>

        </Container>
    );
});
export default Auth;