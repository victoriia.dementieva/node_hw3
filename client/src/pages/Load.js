import { observer } from "mobx-react-lite";
import { Button, Col, Container, Image, Row } from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import img from '../assets/load.jpg';
import { useParams, useNavigate } from "react-router-dom";
import { deleteLoadById } from "../http/loadAPI";
import { LOAD_ROUTE } from '../utils/constants';
import { useTranslation } from "react-i18next";

const Load = observer(() => {
    const { id } = useParams();
    const navigate = useNavigate();
    const { t } = useTranslation();

    return (
        <Container>
            <Row style={{
                border: '1px solid #fff',
                borderRadius: '50px',
                background: "linear-gradient(to left,#ba915b, #fff)"
            }}
                className={'d-flex justify-content-between'}>
                <div className={'col-3'}>
                    <Image width={300} height={300}
                        src={img} />
                </div>

                <Col className={'text-center col-8 p-4 '}>
                    <h2>{`Load № ${id}`}</h2>
                    <Form.Control
                        value={`text`}
                        disabled />
                </Col>
                <Col>
                    <Button>{t('load.edit')}</Button>
                    <Button>{t('load.post')}</Button>
                    <Button onClick={() => {
                        deleteLoadById(id);
                        navigate(LOAD_ROUTE);
                    }}>{t('load.delete')}</Button>

                </Col>
            </Row>
        </Container>
    );
});
export default Load;