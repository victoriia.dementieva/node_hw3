// import User from "./pages/User";
import TrucksList from "./pages/TrucksList";
import LoadsList from "./pages/LoadsList";
import Truck from "./pages/Truck";
import Load from "./pages/Load";
import Auth from "./pages/Auth";
import Main from "./pages/Main";
import {
    MAIN_ROUTE,
    LOGIN_ROUTE,
    TRUCK_ROUTE,
    LOAD_ROUTE,
    REGISTRATION_ROUTE,
    FORGOT_ROUTE,
    USER_ROUTE
} from "./utils/constants";
import User from "./pages/User";

export const authRoutes = [
    {
        path: TRUCK_ROUTE,
        Component: <TrucksList />
    },
    {
        path: TRUCK_ROUTE + '/:id',
        Component: <Truck />
    },
    {
        path: LOAD_ROUTE,
        Component: <LoadsList />
    },
    {
        path: LOAD_ROUTE + '/:id',
        Component: <Load />
    },
    {
        path: USER_ROUTE,
        Component: <User />
    },

];

export const publicRoutes = [
    {
        path: MAIN_ROUTE,
        Component: <Main />
    },
    {
        path: LOGIN_ROUTE,
        Component: <Auth />
    },
    {
        path: REGISTRATION_ROUTE,
        Component: <Auth />
    },
    {
        path: FORGOT_ROUTE,
        Component: <Auth />
    },
];