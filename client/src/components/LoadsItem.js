import { Card, Col, Image } from "react-bootstrap";
import img from '../assets/load.jpg';
import { LOAD_ROUTE } from "../utils/constants";
import { useNavigate } from "react-router-dom";

const LoadsItem = ({ loads }) => {
    const navigate = useNavigate();

    return (
        <Col
            md={2}
            onClick={() => navigate(`${LOAD_ROUTE}/${loads._id}`)}>
            <Card
                style={{
                    width: 150,
                    cursor: 'pointer'
                }}
                className='text-center 
                d-flex justify-content-center 
                align-items-center p-4 mb-4'>
                <Image
                    width={100}
                    height={100}
                    src={img}
                    className='mb-2' />
                <div style={{fontSize:"10px"}} className="text-center ">
                    {loads._id}
                </div>
            </Card>
        </Col>
    );
};
export default LoadsItem;
