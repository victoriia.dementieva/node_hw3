import { observer } from "mobx-react-lite";
import { useContext } from 'react';
import { Row } from "react-bootstrap";
import { Context } from "..";
import LoadsItem from "./LoadsItem";

const LoadsBar = observer(() => {
    const { loads } = useContext(Context);
    return (
        <Row className="d-flex col-10 mx-auto">
            {loads.loads?.map((load) => {
                return <LoadsItem
                    key={load._id}
                    loads={load}
                />
            })
            }
        </Row>
    );
});
export default LoadsBar;