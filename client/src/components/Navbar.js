import Container from 'react-bootstrap/Container';
import {Nav, Form} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import React, { useContext } from 'react';
import { Context } from '..';
import { LOGIN_ROUTE, MAIN_ROUTE, USER_ROUTE } from '../utils/constants';
import { Button } from 'react-bootstrap';
import { observer } from 'mobx-react-lite';
import { useNavigate } from "react-router-dom";
import {useTranslation} from "react-i18next";

const NavBar = observer(() => {
    const { user } = useContext(Context);
    const navigate = useNavigate();
    const { t, i18n } = useTranslation();
    const logOut = () => {
        user.setUser({});
        user.setIsAuth(false);
    }

    return (
        <Navbar bg="light" variant="light">
            <Container>
                <Navbar.Brand href={MAIN_ROUTE}>{t('brand')}</Navbar.Brand>
                <Form.Select className="w-25" onChange={(value) => {
                    i18n.changeLanguage(value.target.value);
                }}>
                    <option value="en">English</option>
                    <option value="ua">Українська</option>
                </Form.Select>
                {
                    user.isAuth ?
                        <Nav className="ml-auto">
                            <Button style={{
                                background: '#a7c4af',
                                border: 'none'
                            }}
                                onClick={() => navigate(USER_ROUTE)}>{t('auth.profile')}</Button>
                            <Button style={{
                                background: '#a7c4af',
                                border: 'none'
                            }}
                                className="ml-5"
                                onClick={() => {
                                    logOut();
                                }}>{t('auth.logout')}</Button>
                        </Nav> :
                        <Nav className="ml-auto">
                            <Button style={{
                                background: '#a7c4af',
                                border: 'none'
                            }} onClick={() => {
                                user.setIsAuth(true);
                                navigate(LOGIN_ROUTE);
                            }}>{t('auth.login')}</Button>
                        </Nav>}
            </Container>
        </Navbar >
    );
});

export default NavBar;



