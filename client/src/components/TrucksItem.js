import { Card, Col, Image } from "react-bootstrap";
import img from '../assets/truck.png';
import { TRUCK_ROUTE } from "../utils/constants";
import { useNavigate } from "react-router-dom";

const TrucksItem = ({ trucks }) => {
    const navigate = useNavigate();
    return (
        <Col
            md={2}
            onClick={() => navigate(`${TRUCK_ROUTE}/${trucks._id}`)}>
            <Card
                style={{
                    width: 150,
                    cursor: 'pointer'
                }}
                className='text-center 
                d-flex justify-content-center 
                align-items-center p-4 mb-4'>
                <Image
                    width={100}
                    height={100}
                    src={img}
                    className='mb-2' />
                <div style={{fontSize:"10px"}} className="text-center ">
                    {trucks._id}
                </div>
            </Card>
        </Col>
    );
};
export default TrucksItem;
