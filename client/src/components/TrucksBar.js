import { observer } from "mobx-react-lite";
import { useContext, useEffect } from "react";
import { Row } from "react-bootstrap";
import { Context } from "..";
import TrucksItem from "./TrucksItem";


const TrucksBar = observer(() => {
    const { trucks } = useContext(Context);
    return (
        <Row className="d-flex col-10 mx-auto">
            {trucks.trucks?.map((truck) => {
                return <TrucksItem
                    key={truck._id}
                    trucks={truck}
                />
            })
            }
        </Row>
    );
});
export default TrucksBar;
