import { $host, $authHost } from './index';
import jwt_decode from 'jwt-decode';

export const registration = async (email, password, role) => {
    const { data } = await $host.post('api/auth/register', { email, password, role });
    return data;
}
export const login = async (email, password) => {
    const { data } = await $host.post('api/auth/login', { email, password });
    console.log(data);
    localStorage.setItem('token', data.jwt_token);
    return jwt_decode(data.jwt_token);
}
export const check = async () => {
    const { data } = await $authHost.post('api/auth/register')
    localStorage.setItem('token', data.jwt_token);
    return jwt_decode(data.jwt_token);
}

export const getUserProfile = async () => {
    const { data } = await $authHost.get('api/users/me');
    return data;
}

export const deleteUserProfile = async () => {
    const { data } = await $authHost.delete('api/users/me');
    return data;
}

export const changePassword = async () => {
    const { data } = await $authHost.patch('api/users/me/password');
    return data;
}