import { $host, $authHost } from './index';


export const createTruck = async (truck) => {
    const { data } = await $authHost.post('api/trucks', { truck });
    return data;
}

export const getTrucks = async () => {
    const { data } = await $authHost.get('api/trucks');
    return data;
}

export const getTruckByID = async (id) => {
    const { data } = await $authHost.get(`api/trucks/${id}`);
    return data;
}

export const deleteTruckById = async (id) => {
    const { data } = await $authHost.delete(`api/trucks/${id}`);
    return data;
}