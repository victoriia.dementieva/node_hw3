import { $host, $authHost } from './index';


export const createLoad = async (load) => {
    const { data } = await $authHost.post('api/loads', { load });
    return data;
}

export const getLoads = async () => {
    const { data } = await $authHost.get('api/loads');
    return data;
}

export const getLoadByID = async (id) => {
    const { data } = await $authHost.get(`api/loads/${id}`);
    return data;
}

export const deleteLoadById = async (id) => {
    const { data } = await $authHost.delete(`api/loads/${id}`);
    return data;
}