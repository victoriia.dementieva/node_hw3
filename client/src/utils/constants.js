export const MAIN_ROUTE = '/';

export const LOGIN_ROUTE = '/login';

export const REGISTRATION_ROUTE = '/register';

export const FORGOT_ROUTE = '/forgot_password';

export const LOAD_ROUTE = '/loads';

export const TRUCK_ROUTE = '/trucks';

export const USER_ROUTE = 'users/me';