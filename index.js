const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const PORT = process.env.PORT || 8080;
require('dotenv').config();
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.connect('mongodb+srv://viktoriiadmntvva:ED7XR1XAlKFCbURM@cluster0.ottiemf.mongodb.net/trucks?retryWrites=true&w=majority');

const { authRouter } = require('./src/view/authRouter.js');
const { loadRouter } = require('./src/view/loadRouter.js');
const { truckRouter } = require('./src/view/truckRouter.js');
const { userRouter } = require('./src/view/userRouter.js');


app.use(cors());
app.use(express.json());

let accessLog = fs.createWriteStream(path.join(__dirname, 'requestInfo.log'),
    {
        flags: 'a'
    });

app.use(morgan('tiny', {
    stream: accessLog
}));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);


app.listen(PORT, () => { console.log(`port: ${PORT}`); });

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
    res.status(500).send({ message: err.message });
}
